const setCache = data => spotify_data.put('lastPlayed', data)
const getCache = () => spotify_data.get('lastPlayed')
const getRefresh = () => spotify_data.get('refresh_token')
const setRefreshToken = token => spotify_data.put('refresh_token', token)
const scope = 'user-read-currently-playing, user-read-recently-played, user-library-modify'
const redirect_uri = 'https://spotify.playstateprojects.com/callback'

function getParameterByName(name, url) {
  name = name.replace(/[\[\]]/g, '\\$&')
  name = name.replace(/\//g, '')
  var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
    results = regex.exec(url)

  if (!results) return null
  else if (!results[2]) return ''
  else if (results[2]) {
    results[2] = results[2].replace(/\//g, '')
  }

  return decodeURIComponent(results[2].replace(/\+/g, ' '))
}

addEventListener('fetch', event => {
  event.respondWith(handleRequest(event.request))
})
/**
 * Respond with hello worker text
 * @param {Request} request
 */
async function handleRequest(request) {
  var uri = request.url.replace(/^https:\/\/.*?\//gi, '/')
  if (uri == '/login') {
    return loginRedirect()
  } else if (uri.indexOf('callback') > -1) {
    const code = getParameterByName('code', request.url)
    return handleCallBack(code)
  } else if(uri.indexOf('history') > -1){
	const tokenResponse = await refresh(request)
    const body = await tokenResponse.json()
	  return periouslyPlayed(body.access_token);
  }else {
    const tokenResponse = await refresh(request)
    const body = await tokenResponse.json()
    console.log(body)
    return nowPlaying(body.access_token)
  }
}
function loginRedirect() {
  let query = 'https://accounts.spotify.com/authorize'
  query += '?response_type=code'
  query += '&client_id=' + CLIENT_ID
  query += '&scope=' + scope
  query += '&redirect_uri=' + encodeURI(redirect_uri)
  return Response.redirect(query, 302)
}
async function handleCallBack(code) {
  let codeResponse = await getToken(code)
  let codes = await codeResponse.json()
  setRefreshToken(codes.refresh_token)
  spotify_data.put('refresh', codes.refresh_token)
  let res = { codes: codes, eetoken: codes.refresh_token }
  return new Response(JSON.stringify(res), {
    headers: {
      'Content-Type': 'application/json',
      'access-control-allow-headers':
        'Accept, App-Platform, Authorization, Content-Type, Origin, Retry-After, Spotify-App-Version, X-Cloud-Trace-Context',
      'access-control-allow-methods': 'GET, POST, OPTIONS, PUT, DELETE, PATCH',
      'access-control-allow-origin': '*',
    },
    status: 200,
  })
}
async function nowPlaying(token) {
  let spotifyReq = await fetch(
    'https://api.spotify.com/v1/me/player/currently-playing',
    {
      method: 'GET',
      headers: { Authorization: 'Bearer ' + token },
    },
  )

  if (spotifyReq.status == 200) {
    let info = await spotifyReq.json()
    setCache(JSON.stringify(info))
    return new Response(JSON.stringify(info), spotifyReq)
  } else {
    let cachedSong = await getCache()
    song = JSON.parse(cachedSong)
    song.is_playing = false
    return new Response(JSON.stringify(song), {
      headers: {
        'Content-Type': 'application/json',
        'access-control-allow-headers':
          'Accept, App-Platform, Authorization, Content-Type, Origin, Retry-After, Spotify-App-Version, X-Cloud-Trace-Context',
        'access-control-allow-methods':
          'GET, POST, OPTIONS, PUT, DELETE, PATCH',
        'access-control-allow-origin': '*',
      },
      status: 200,
    })
  }
}
async function periouslyPlayed(token) {
  let spotifyReq = await fetch(
    'https://api.spotify.com/v1/me/player/recently-played',
    {
      method: 'GET',
      headers: { Authorization: 'Bearer ' + token },
    },
  )
	console.log(spotifyReq.body);
  if (spotifyReq.status == 200) {

	let info = await spotifyReq.json();
	console.log('v',info)
    return new Response(JSON.stringify(info), spotifyReq)
  }else{
	  console.log('ccc',token);
	return new Response(JSON.stringify(spotifyReq),spotifyReq)
  }

}
async function refresh(request) {
  let refresh = await getRefresh();
  const params = {
    grant_type: 'refresh_token',
    refresh_token: refresh,
  }
  const formBody = Object.keys(params)
    .map(key => encodeURIComponent(key) + '=' + encodeURIComponent(params[key]))
    .join('&')
  let response = await fetch('https://accounts.spotify.com/api/token', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      Authorization:
        'Basic ' + btoa(CLIENT_ID + ':' + CLIENT_SECRET).toString('base64'),
    },
    body: formBody,
  })
  return new Response(response.body, response)
}

async function getToken(code) {
  const params = {
    grant_type: 'authorization_code',
    code: code,
    redirect_uri: redirect_uri,
  }
  const formBody = Object.keys(params)
    .map(key => encodeURIComponent(key) + '=' + encodeURIComponent(params[key]))
    .join('&')
  let response = await fetch('https://accounts.spotify.com/api/token', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      Authorization:
        'Basic ' + btoa(CLIENT_ID + ':' + CLIENT_SECRET).toString('base64'),
    },
    body: formBody,
  })

  return new Response(response.body, response)
}
